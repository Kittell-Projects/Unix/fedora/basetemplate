#!/bin/sh

#  Base.sh
#
#
#  Created by David Kittell on 7/7/19.
#

clear

echo -e "\033[32mFedora - Base Setup - Start\033[0m"

echo -e "\033[32mFedora - Install / Enable SSH - Start\033[0m"
sudo dnf install openssh-server -y
sudo systemctl start sshd.service
sudo systemctl enable sshd.service
echo -e "\033[32mFedora - Install / Enable SSH - Stop\033[0m"

echo -e "\033[32mChecking UNIX Identification\033[0m"
if [ -f /opt/UnixIdentification.sh ]; then
  sudo rm -f /opt/UnixIdentification.sh
  sudo wget -P /opt/ https://gitlab.com/Kittell-Projects/Unix/unixidentification/raw/master/UnixIdentification.sh
else
  sudo wget -P /opt/ https://gitlab.com/Kittell-Projects/Unix/unixidentification/raw/master/UnixIdentification.sh
fi

sh /opt/UnixIdentification.sh -d
case "$DISTRO_ID" in
  ${DISTRO_ID_FEDORA})
    vRHEL=$(sh /opt/UnixIdentification.sh -r)
    # echo $vRHEL
    ;;
  *)
    echo "$DISTRO_ID\n$DISTRO_DESC not supported"
    exit
    ;;
esac

clear
echo -e "\033[32mSet Server Hostname - Start\033[0m"
if [ -z ${3+x} ]; then
  echo -e "\033[01m\e[4mType your desired hostname for the server, followed by [ENTER]:\e[0m\033[0m"
  read hostname
  sudo hostnamectl set-hostname $hostname
else
  declare hostname=$3
  sudo hostnamectl set-hostname $hostname
fi
echo -e "\033[32mSet Server Hostname - Stop\033[0m"

sudo dnf repolist all # Typically not needed

echo -e "\033[32mCreating dnf Cache\033[0m"
sudo dnf -y makecache

echo -e "\033[32mRunning dnf Update\033[0m"
sudo dnf -y update # Make sure you are up to date before you start

echo -e "\nCurrent timezone is $(timedatectl | grep "Time zone" | cut -d ":" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')"
echo -e "\033[01m\e[4mDo you need to set/change the timezone, if yes type y or if no type n followed by [ENTER]:\e[0m\033[0m"
read tzSet

case $tzSet in
  [yY])
    echo -e "\nTimezone should be in format similar to 'America/Detroit'"
    echo -e "\033[01m\e[4mType your desired timezone for the server, followed by [ENTER]:\e[0m\033[0m"
    read tzNew
    echo -e "\033[32mSetting Timezone\033[0m"
    sudo timedatectl set-timezone $tzNew # Optional but make sure you have the correct timezone
    ;;
  *)
    echo -e "\nTimezone is $(timedatectl | grep "Time zone" | cut -d ":" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')"
    ;;
esac

# Install needed applications - start
echo -e "\033[32mInstalling Needed Applications\033[0m"
sudo dnf -y install net-tools bind-utils nano wget tcl unzip bzip2 ed gcc make libstdc++.so.6 telnet
# Install needed applications - stop
